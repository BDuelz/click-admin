from clickadmin import create_app
from werkzeug.debug import DebuggedApplication


app = create_app()

if app.debug:
    app.wsgi_app = DebuggedApplication(app.wsgi_app, True)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
