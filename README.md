# Click Admin

A research platform allowing network administrators to observe how people use their network when they're given the choice to decide how network resources are allocated. The platform is built on top of The Click Modular Router, which means network administrators can make full use of Click.

## Prerequisites

* [Ansible](http://www.ansible.com)
* [Vagrant](http://www.vagrantup.com)
* [Virtualbox](https://www.virtualbox.org)

## Configuration

* Configuration settings are set in `deployment/vars.yaml`. In the least, the list of default administrators should be chaged and/or added to.
* Create `deployment/secrets/vars.yaml` using `deployment/secrets/vars.yaml.example` as a reference and following the instructions found in the comments.

## Installation

* After installing vagrant and ansible, run `vagrant up` to install Click Admin on a virtual machine.
* On the machine, run `source ~/envs/click-admin/bin/activate` to activate the virtual environment.
* Run `python ~/apps/click-admin/manage.py db upgrade` to setup the database.
* Run `python ~/apps/click-admin/manage.py db seed` to seed the database. This will create admin accounts for all email addresses listed in `deployment/vars.yaml` with a default password of `password`. Log in and change this immediately.

## License

[MIT Licensed](http://opensource.org/licenses/MIT)

Copyright (c) 2013 Abdul Hassan