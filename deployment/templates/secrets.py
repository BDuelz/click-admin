redis_url = 'redis://localhost:6379'

TIMEZONE = '{{ timezone }}'

# For email notifications
ADMINS = {{ admins }}

# Location of Click router configuration
ROUTER_CONFIG_FILE = '{{ router_config_file }}'

# Flask-Core
SECRET_KEY = '{{ secret_key }}'

# Flask-Mail
MAIL_DEFAULT_SENDER = 'noreply@{{ domain }}'

# Flask-Security
SECURITY_PASSWORD_SALT = '{{ security_password_salt }}'

# Flask-SQLAlchemy
SQLALCHEMY_DATABASE_URI = 'postgresql://{{ db_user }}:{{ db_pass }}@localhost/{{ db_name }}'