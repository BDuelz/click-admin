# Production configuration
from clickadmin.config.common import *


# Flask-Core
DEBUG = False

# Flask-Cache
CACHE_DEFAULT_TIMEOUT = 5 * 60

# Flask-WTF
CSRF_ENABLED = True