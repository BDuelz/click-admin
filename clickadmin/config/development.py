# Development configuration
from clickadmin.config.common import *


# Flask-Core
DEBUG = True

# Flask-Cache
CACHE_DEFAULT_TIMEOUT = 1 * 60

# Flask-WTF
CSRF_ENABLED = False