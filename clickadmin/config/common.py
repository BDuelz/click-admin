import os
from celery.schedules import crontab
from clickadmin.config.secrets import *
from datetime import timedelta


PROJECT_NAME = __name__.split('.')[0]

DEFAULT_CONFIGURATION = '''
// Pass packets along untouched
FromDevice(eth0) -> Queue -> ToDevice(eth0);
'''

# Configuration files can run no longer than this many hours
MAX_CONFIGURATION_DURATION = 24

# Minimum time before the start of a new experiment
MIN_CREATE_EXPERIMENT_TD = timedelta(days=1)

# Maximum time before the start of a new experiment
MAX_CREATE_EXPERIMENT_TD = timedelta(days=30)

# When host information gets past this age, prompt user to update
MAX_OUTDATED_HOST_TD = timedelta(days=14)

# First notification is sent this far in advance to the start of a trial
FIRST_NOTIFICATION_TD = timedelta(hours=23)

# Second notification is sent this far in advance to the start of a trial
SECOND_NOTIFICATION_TD = timedelta(hours=3)

# Directories
BASE_DIR = os.path.abspath(os.path.dirname(os.path.dirname(os.path.dirname(__file__))))
INSTANCE_PATH = os.path.join(BASE_DIR, 'instance')
LOG_PATH = os.path.join(INSTANCE_PATH, 'logs')

# Flask Core
TESTING = False
MAX_CONTENT_LENGTH = 16 * 1024 * 1024

# Flask-Cache
CACHE_TYPE = 'redis'
CACHE_REDIS_URL = redis_url

# Flask-Mail
MAIL_SERVER = 'localhost'
MAIL_PORT = 25

# Celery
BROKER_URL = redis_url
CELERY_RESULT_BACKEND = redis_url
CELERY_IMPORTS = ['clickadmin.tasks']
CELERY_TIMEZONE = TIMEZONE
CELERY_IGNORE_RESULT = False
CELERY_SEND_TASK_ERROR_EMAILS = True
EMAIL_HOST = MAIL_SERVER
EMAIL_PORT = MAIL_PORT
SERVER_EMAIL = MAIL_DEFAULT_SENDER

CELERYBEAT_SCHEDULE = {
    'check-next-experiment-for-errors': {
        'task': 'check-next-experiment-for-errors',
        'schedule': crontab(minute=0, hour='*/1'),
    },
    'send-survey-reminders': {
        'task': 'send-survey-reminders',
        'schedule': crontab(minute=0, hour='*/1'),
    },
    'update-router-configuration': {
        'task': 'update-router-configuration',
        'schedule': crontab(minute=0, hour='*/1'),
    },
}

# Flask-DebugToolbar
DEBUG_TB_INTERCEPT_REDIRECTS = False
DEBUG_TB_PROFILER_ENABLED = True

# Flask-Security
SECURITY_EMAIL_SENDER = MAIL_DEFAULT_SENDER
SECURITY_PASSWORD_HASH = 'bcrypt'
SECURITY_REGISTERABLE = True
SECURITY_RECOVERABLE = True
SECURITY_TRACKABLE = True
SECURITY_CHANGEABLE = True
SECURITY_SEND_REGISTER_EMAIL = False
SECURITY_DEFAULT_REMEMBER_ME = True
SECURITY_POST_LOGIN_VIEW = 'users.update_host'
SECURITY_POST_LOGOUT_VIEW = 'security.login'
SECURITY_POST_CHANGE_VIEW = 'index'
SECURITY_LOGIN_URL = '/login/'
SECURITY_LOGOUT_URL = '/logout/'
SECURITY_REGISTER_URL = '/register/'
SECURITY_RESET_URL = '/reset/'
SECURITY_CHANGE_URL = '/password/'

# Flask-SQLAlchemy
SQLALCHEMY_ECHO = False
SQLALCHEMY_COMMIT_ON_TEARDOWN = True

# Flask-WTF
CSRF_ENABLED = True