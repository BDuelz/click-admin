# Testing configuration
from clickadmin.config.common import *


# Flask-Core
DEBUG = True
TESTING = True

# Flask-Cache
CACHE_TYPE = 'null'
CACHE_NO_NULL_WARNING = True

# Flask-SQLAlchemy
SQLALCHEMY_ECHO = True
SQLALCHEMY_DATABASE_URI = 'sqlite:///:memory:'

# Flask-WTF
CSRF_ENABLED = False