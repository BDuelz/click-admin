import re
from clickadmin.extensions import db
from datetime import datetime, timedelta
from wtforms.fields import TextAreaField, TextField
from wtforms.widgets import TextArea, TextInput


def parse_timedelta(s):
    '''
    Create timedelta object from timedelta string representation.
    '''
    if not s:
        return None
    match = re.match(r'((?P<days>\d+) days?, )?(?P<hours>\d+):(?P<minutes>\d+):(?P<seconds>\d+)', str(s))
    if not match:
        return None
    d = match.groupdict(0)
    return timedelta(**dict([(key, int(value)) for key, value in d.items()]))


class Model(object):
    '''
    Base model mixin.
    
    Encapsulates all use of the `query` object.
    '''
    
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.now)
    
    def __str__(self):
        return unicode(self).encode('utf-8')
    
    def __repr__(self):
        return repr(unicode(self))
    
    def __unicode__(self):
        return self.__class__.__name__
    
    @classmethod
    def all(cls):
        return cls.query.all()
    
    @classmethod
    def get(cls, ident):
        return cls.query.get(ident)
    
    @classmethod
    def get_or_404(cls, ident):
        return cls.query.get_or_404(ident)
    
    @classmethod
    def first_or_404(cls):
        return cls.query.first_or_404()
    
    @classmethod
    def paginate(cls, page, per_page, error_out=True):
        return cls.query.paginate(page, per_page, error_out)

    @classmethod
    def create(cls, **kwargs):
        obj = cls(**kwargs)
        db.session.add(obj)
        db.session.flush()
        return obj
    
    def delete(self):
        db.session.delete(self)


class CKTextAreaWidget(TextArea):
    '''
    Renders textarea field in context of CKEditor.
    '''

    def __call__(self, field, **kwargs):
        kwargs.setdefault('class_', 'ckeditor')
        return super(CKTextAreaWidget, self).__call__(field, **kwargs)


class CKTextAreaField(TextAreaField):

    widget = CKTextAreaWidget()


class ReadOnlyWidget(TextInput):
    '''
    Renders disabled input text field.
    '''
    
    def __call__(self, field, **kwargs):
        kwargs.setdefault('disabled', True)
        kwargs.setdefault('class_', 'uneditable-input')
        return super(ReadOnlyWidget, self).__call__(field, **kwargs)


class ReadOnlyField(TextField):
    
    widget = ReadOnlyWidget()