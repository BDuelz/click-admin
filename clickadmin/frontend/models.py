from clickadmin.extensions import db
from clickadmin.utils import Model
from flask import abort
from sqlalchemy import event


class Page(Model, db.Model):
    '''
    Pages allow administrators to create/edit custom html pages.
    '''

    __tablename__ = 'pages'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(80), nullable=False, unique=True)
    title = db.Column(db.Unicode(255), nullable=False)
    content = db.Column(db.UnicodeText)
    permanent = db.Column(db.Boolean, nullable=False, default=False)

    def __unicode__(self):
        return self.name

    @classmethod
    def get_by_name(cls, name):
        return cls.query.filter(db.func.lower(cls.name)==db.func.lower(name)).first()

    @classmethod
    def get_by_name_or_404(cls, name):
        rv = cls.get_by_name(name)
        if rv is None:
            abort(404)
        return rv


@event.listens_for(Page, 'before_delete')
def prevent_delete(mapper, connection, target):
    if target.permanent:
        raise Exception('%s page is permanent.' % target.name.capitalize())