from clickadmin.frontend import Page
from flask import Blueprint, render_template


bp = Blueprint('frontend', __name__)


def index():
    return page('home')


@bp.route('/<path:name>/')
def page(name=None):
    page = Page.get_by_name_or_404(name)
    return render_template('frontend/page.html', page=page)