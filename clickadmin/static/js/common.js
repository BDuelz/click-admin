$(document).ready(function () {
    $('form :input:enabled:visible:first').focus();

	var hash = window.location.hash.substring(1);

	if (hash) {
	    $('.tabbable .nav-tabs a[href=#' + hash + ']').tab('show');
	}

	$('.tabbable .nav-tabs a').click(function () {
        $(this).tab('show');
        return false;
    });

	$('.tabbable .nav-tabs a').on('shown', function (e) {
	    window.location.hash = e.target.hash;
	});
});