from clickadmin.experiments import (Configuration, Experiment, Option, Survey,
    UserChoice)
from clickadmin.extensions import db
from clickadmin.frontend import Page
from clickadmin.users import Role, User, UserIpAddress, UserMacAddress
from clickadmin.utils import CKTextAreaField, ReadOnlyField, parse_timedelta
from datetime import datetime, timedelta
from flask import current_app as app
from flask.ext.admin import Admin, AdminIndexView as _AdminIndexView
from flask.ext.admin.base import MenuLink
from flask.ext.admin.contrib.sqla import ModelView as _ModelView
from flask.ext.security import current_user
from humanize.time import naturaldelta
from webhelpers.text import urlify
from wtforms import validators
from wtforms.fields import SelectField


def validate_start_time(form, field):
    start_time = field.data
    if not start_time:
        return
    experiment = form._obj
    if not experiment:
        raise validators.ValidationError('Cannot schedule experiment at time of creation.')
    if experiment.duration < timedelta(hours=1):
        raise validators.ValidationError('Cannot schedule experiment - unacceptable duration (add a configuration).')
    delta = start_time - datetime.now()
    if delta < timedelta(0):
        raise validators.ValidationError('Cannot schedule experiment to start in the past.')
    if timedelta(0) < delta < app.config['MIN_CREATE_EXPERIMENT_TD']:
        if not experiment.start_time:
            raise validators.ValidationError('Cannot schedule experiment within %s of expected start time.' % naturaldelta(app.config['MIN_CREATE_EXPERIMENT_TD']))
        elif experiment.start_time != start_time:
            raise validators.ValidationError('Cannot reschedule experiment to be within %s of expected start time.' % naturaldelta(app.config['MIN_CREATE_EXPERIMENT_TD']))
    if app.config['MAX_CREATE_EXPERIMENT_TD'] < delta:
        raise validators.ValidationError('Cannot schedule experiment further than %s of expected start time.' % naturaldelta(app.config['MAX_CREATE_EXPERIMENT_TD']))
    if not start_time.minute == start_time.second == start_time.microsecond == 0:
        raise validators.ValidationError('Cannot schedule experiment - must start at the top of the hour.')
    end_time = start_time + experiment.duration
    if Experiment.get_experiments_in_date_range(start_time, end_time, experiment):
        raise validators.ValidationError('Cannot schedule experiment - date conflicts with an existing experiment.')


def validate_previous_configuration(form, field):
    experiment = form.experiment.data
    previous_configuration = form.previous_configuration.data
    if previous_configuration and experiment is not previous_configuration.experiment:
        raise validators.ValidationError('Does not belong to the same experiment.')


def validate_duration(form, field):
    duration = field.data
    if duration < timedelta(hours=1):
        raise validators.ValidationError('Unacceptable duration - must be at least one hour.')
    if duration.seconds % 3600 != 0:
        raise validators.ValidationError('Unacceptable duration - can only be set in hour increments.')


class ProtectedView(object):

    def is_accessible(self):
        return current_user.has_role('admin')


class AdminIndexView(ProtectedView, _AdminIndexView):

    def __init__(self, **kwargs):
        super(AdminIndexView, self).__init__(name='Admin Home', **kwargs)

    @property
    def _template_args(self):
        experiments = Experiment.get_scheduled_experiments()
        return dict(experiments=experiments)


class ModelView(ProtectedView, _ModelView):

    column_display_pk = True
    column_default_sort = 'created_at'
    create_template = 'admin/create.html'
    edit_template = 'admin/edit.html'

    def __init__(self, model, **kwargs):
        self.column_list = self.column_list or []
        self.column_list.append('created_at')
        self.column_filters = self.column_filters or []
        self.column_filters.append('created_at')
        self.column_sortable_list = self.column_sortable_list or []
        self.column_sortable_list.append('created_at')
        self.form_excluded_columns = self.form_excluded_columns or []
        self.form_excluded_columns.append('created_at')
        super(ModelView, self).__init__(model, db.session, **kwargs)


class PageView(ModelView):

    column_list = ['id', 'name', 'title']
    column_descriptions = dict(name='Also used as URL of page.')
    column_filters = ['name', 'title', 'content']
    column_searchable_list = ['name', 'title', 'content']
    form_excluded_columns = ['permanent']
    form_overrides = dict(content=CKTextAreaField)

    form_args = {
        'name': {
            'filters': [lambda value: value and urlify(value)],
        },
    }

    def __init__(self, **kwargs):
        super(PageView, self).__init__(Page, name='Custom Pages', endpoint='page', **kwargs)


class RoleView(ModelView):

    column_list = ['id', 'name']
    column_exclude_list = ['description', 'permanent']
    column_filters = ['name', 'description']
    column_searchable_list = ['name', 'description']
    form_excluded_columns = ['permanent', 'users']

    def __init__(self, **kwargs):
        super(RoleView, self).__init__(Role, name='User Roles', endpoint='role', **kwargs)


class UserView(ModelView):

    can_create = False
    can_delete = False
    column_list = ['id', 'email', 'active']
    column_filters = ['email', 'active']
    column_searchable_list = ['email']
    form_excluded_columns = ['password', 'host_last_updated_at', 'confirmed_at',
                             'last_login_at', 'current_login_at', 'last_login_ip',
                             'current_login_ip', 'login_count', 'ip_addresses',
                             'mac_addresses']
    form_overrides = dict(email=ReadOnlyField)

    def __init__(self, **kwargs):
        super(UserView, self).__init__(User, name='Users', endpoint='user', **kwargs)


class UserIpAddressView(ModelView):

    can_create = False
    can_edit = False
    can_delete = False
    column_list = ['id', 'user', 'ip_address']
    column_filters = [User.email, 'ip_address']
    column_searchable_list = [User.email, 'ip_address']
    column_sortable_list = [('user', User.email)]

    def __init__(self, **kwargs):
        super(UserIpAddressView, self).__init__(UserIpAddress, name='User Ip Addresses', endpoint='useripaddress', **kwargs)


class UserMacAddressView(ModelView):

    can_create = False
    can_edit = False
    can_delete = False
    column_list = ['id', 'user', 'mac_address']
    column_filters = [User.email, 'mac_address']
    column_searchable_list = [User.email, 'mac_address']
    column_sortable_list = [('user', User.email)]

    def __init__(self, **kwargs):
        super(UserMacAddressView, self).__init__(UserMacAddress, name='User Mac Addresses', endpoint='usermacaddress', **kwargs)


class ExperimentView(ModelView):

    column_list = ['id', 'title', 'admin_duration', 'start_time', 'end_time',
                   'admin_has_valid_surveys', 'admin_has_valid_configurations',
                   'admin_configuration_order']
    column_labels = dict(admin_duration='Duration',
                         admin_has_valid_surveys='Has Valid Surveys',
                         admin_has_valid_configurations='Has Valid Configurations',
                         admin_configuration_order='Configuration Order')
    column_descriptions = dict(admin_configuration_order='Specified order to run configuration files.')
    column_filters = ['title', 'description', 'start_time', 'end_time']
    column_searchable_list = ['title', 'description']
    column_sortable_list = ['id', 'title', 'start_time', 'end_time']
    form_excluded_columns = ['duration', 'end_time', 'configurations', 'surveys']
    form_overrides = dict(description=CKTextAreaField)

    form_args = {
        'start_time': {
            'validators': [validate_start_time],
        },
    }

    def __init__(self, **kwargs):
        super(ExperimentView, self).__init__(Experiment, name='Experiments', endpoint='experiment', **kwargs)


class SurveyView(ModelView):

    column_list = ['id', 'experiment', 'question', 'admin_is_valid']
    column_labels = dict(admin_is_valid='Is Valid')
    column_descriptions = dict(admin_is_valid='Whether there are an acceptable number of unique options.')
    column_filters = [Experiment.title, 'question']
    column_searchable_list = [Experiment.title, 'question']
    column_sortable_list = ['id', 'question', ('experiment', Experiment.title)]
    form_excluded_columns = ['options']
    form_overrides = dict(description=CKTextAreaField)

    def __init__(self, **kwargs):
        super(SurveyView, self).__init__(Survey, name='Surveys', endpoint='survey', **kwargs)


class OptionView(ModelView):

    column_list = ['id', 'survey', 'response']
    column_labels = dict(survey='Question')
    column_filters = [Survey.question, 'response']
    column_searchable_list = [Survey.question, 'response']
    column_sortable_list = ['id', 'response', ('survey', Survey.question)]

    def __init__(self, **kwargs):
        super(OptionView, self).__init__(Option, name='Survey Options', endpoint='option', **kwargs)


class ConfigurationView(ModelView):

    column_list = ['id', 'previous_configuration', 'experiment', 'admin_duration',
                   'admin_is_valid']
    column_labels = dict(previous_configuration='Previous Id', text='Content',
                         admin_duration='Duration', admin_is_valid='Is Valid')
    column_descriptions = dict(duration='How long configuration file runs.',
                               previous_configuration='Configuration files run in a specified order.',
                               admin_is_valid='Whether the configuration is renderable.')
    column_filters = [Experiment.title, 'text']
    column_searchable_list = [Experiment.title, 'text']
    column_sortable_list = ['id', 'duration', ('experiment', Experiment.title)]
    form_excluded_columns = ['next_configuration']
    form_overrides = dict(content=CKTextAreaField, duration=SelectField)

    form_args = {
        'previous_configuration': {
            'validators': [validate_previous_configuration],
        },
        'duration': {
            'coerce': parse_timedelta,
            'validators': [validate_duration],
        },
    }

    def __init__(self, app, **kwargs):
        self.form_args['duration']['choices'] = [(timedelta(hours=x), naturaldelta(timedelta(hours=x)))
                                                 for x in range(1, app.config['MAX_CONFIGURATION_DURATION'] + 1)]
        super(ConfigurationView, self).__init__(Configuration, name='Click Configurations', endpoint='configuration', **kwargs)


class UserChoiceView(ModelView):

    can_create = False
    can_edit = False
    can_delete = False
    column_list = ['user', 'survey', 'option']
    column_labels = dict(option='Response')
    column_filters = [User.email, Survey.question, Option.response]
    column_searchable_list = [User.email, Survey.question, Option.response]
    column_sortable_list = [('experiment', Experiment.id), ('user', User.email),
                            ('survey', Survey.question), ('option', Option.response)]

    def __init__(self, **kwargs):
        super(UserChoiceView, self).__init__(UserChoice, name='Survey Responses', endpoint='userchoice', **kwargs)


SITE_CATEGORY = 'Site Related'
USER_CATEGORY = 'User Related'
EXPERIMENT_CATEGORY = 'Experiment Related'


def configure_admin(app):
    admin = Admin(app, name='Click Admin', index_view=AdminIndexView())
    admin.add_link(MenuLink(name='Site Home', endpoint='index'))
    admin.add_link(MenuLink(name='Logout', endpoint='security.logout'))
    admin.add_view(PageView(category=SITE_CATEGORY))
    admin.add_view(UserView(category=USER_CATEGORY))
    admin.add_view(UserIpAddressView(category=USER_CATEGORY))
    admin.add_view(UserMacAddressView(category=USER_CATEGORY))
    admin.add_view(RoleView(category=USER_CATEGORY))
    admin.add_view(ExperimentView(category=EXPERIMENT_CATEGORY))
    admin.add_view(SurveyView(category=EXPERIMENT_CATEGORY))
    admin.add_view(OptionView(category=EXPERIMENT_CATEGORY))
    admin.add_view(ConfigurationView(app, category=EXPERIMENT_CATEGORY))
    admin.add_view(UserChoiceView(category=EXPERIMENT_CATEGORY))