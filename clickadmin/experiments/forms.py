from clickadmin.experiments.models import Option
from flask.ext.wtf import Form
from wtforms import SelectField, SubmitField
from wtforms.validators import Required


class AnswerSurveyForm(Form):
    
    option_id = SelectField('Answer', validators=[Required()], coerce=int)
    submit = SubmitField('Answer Survey')
    
    def consent_to_option(self, user):
        option = Option.get(self.option_id.data)
        user.consent_to_option(option)