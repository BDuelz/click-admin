from clickadmin.extensions import db
from clickadmin.utils import Model
from datetime import datetime, timedelta
from flask import current_app as app
from humanize.time import naturaldelta
from sqlalchemy.schema import UniqueConstraint
from sqlalchemy import event
from sqlalchemy.orm import Session


class Experiment(Model, db.Model):

    __tablename__ = 'experiments'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Unicode(140), nullable=False, unique=True)
    description = db.Column(db.UnicodeText)
    duration = db.Column(db.Interval, nullable=False, default=timedelta(0))
    start_time = db.Column(db.DateTime)
    end_time = db.Column(db.DateTime)

    def __unicode__(self):
        return self.title

    @property
    def admin_duration(self):
        '''
        Natural representation of the duration.
        '''
        if not self.duration:
            return 'N/A'
        return naturaldelta(self.duration)

    @property
    def admin_has_valid_surveys(self):
        '''
        Whether all surveys are valid.
        '''
        return self.has_valid_surveys()

    @property
    def admin_has_valid_configurations(self):
        '''
        Whether all configurations are valid.
        '''
        return self.has_valid_configurations()

    @property
    def admin_configuration_order(self):
        '''
        String representation of ordered configurations.
        '''
        if not self.has_valid_configuration_order():
            return 'INVALID'
        return ' ~ '.join([str(c.id) for c in self.ordered_configurations])

    @property
    def ordered_configurations(self):
        '''
        Return configurations in their intended order.

        Note: This does not first check on the validity of the order.
        '''
        if not self.configurations:
            return []
        head_configuration = None
        for configuration in self.configurations:
            if not configuration.previous_configuration:
                head_configuration = configuration
        configurations = []
        current_configuration = head_configuration
        while current_configuration:
            configurations.append(current_configuration)
            current_configuration = current_configuration.next_configuration
        return configurations

    def _get_participating_users_query(self):
        from clickadmin.users import User
        start_time = self.start_time or datetime.now()
        return User.query.filter(User.created_at<start_time)

    @property
    def participating_users(self):
        '''
        Return users who were active when this experiment ran.
        '''
        return self._get_participating_users_query().all()

    @property
    def participating_users_count(self):
        '''
        Return count of users who were active when this experiment ran.
        '''
        return self._get_participating_users_query().count()

    @classmethod
    def get_experiments_in_date_range(cls, start_time, end_time, excluded=None):
        '''
        Return all experiments scheduled in a certain date range.

        @param excluded Experiment object to exclude, useful when searching for
                        other experiments in a certain date range.
        '''
        query = (cls.query
                 .filter(db.or_(cls.start_time.between(start_time, end_time),
                                cls.end_time.between(start_time, end_time))))
        if excluded:
            query = query.filter(cls.id!=excluded.id)
        return query.all()

    @classmethod
    def get_scheduled_experiments(cls):
        '''
        Return all scheduled experiments.
        '''
        return (cls.query
                .filter(cls.start_time!=None)
                .filter(cls.end_time!=None)
                .all())

    @classmethod
    def get_current_experiment(cls):
        '''
        Return any currently running experiment.
        '''
        return (cls.query
                .filter(cls.start_time<datetime.now())
                .filter(cls.end_time>datetime.now())
                .first())

    @classmethod
    def get_next_experiment(cls):
        '''
        Return the next scheduled experiment.
        '''
        return (cls.query
                .filter(cls.start_time>datetime.now())
                .order_by(cls.start_time.asc())
                .first())

    def has_valid_surveys(self):
        '''
        Check whether this experiment has valid surveys.
        '''
        for survey in self.surveys:
            if not survey.is_valid():
                return False
        return True

    def has_valid_configurations(self):
        '''
        Check whether this experiment has valid configurations.
        '''
        for configuration in self.configurations:
            if not configuration.is_valid():
                return False
        return True

    def has_valid_configuration_order(self):
        '''
        Check whether there is an ambiguity in the configuration order.
        '''
        if not self.configurations:
            return True
        head_configuration = None
        for configuration in self.configurations:
            if not configuration.previous_configuration:
                head_configuration = configuration
        if not head_configuration:
            # No head configuration
            return False
        seen_configurations = set()
        current_configuration = head_configuration
        while current_configuration:
            if current_configuration in seen_configurations:
                # Loop found
                return False
            seen_configurations.add(current_configuration)
            current_configuration = current_configuration.next_configuration
        return len(seen_configurations) == len(self.configurations)

    def is_scheduled(self):
        '''
        Check whether this experiment is scheduled.
        '''
        return self.start_time and self.end_time

    def is_past(self):
        '''
        Check whether this experiment has already run.
        '''
        return self.is_scheduled() and self.end_time < datetime.now()

    def is_current(self):
        '''
        Check whether this experiment is currently running.
        '''
        return self.is_scheduled() and self.start_time < datetime.now() < self.end_time

    def is_future(self):
        '''
        Check whether this experiment is scheduled for a future time.
        Unscheduled experiments are considered future experiments.
        '''
        if not self.is_scheduled():
            return True
        return self.start_time > datetime.now()

class Survey(Model, db.Model):

    __tablename__ = 'surveys'
    id = db.Column(db.Integer, primary_key=True)
    experiment_id = db.Column(
        db.Integer, db.ForeignKey('experiments.id', ondelete='cascade'),
        nullable=False)
    question = db.Column(db.Unicode(140), nullable=False)
    description = db.Column(db.UnicodeText)

    experiment = db.relationship(
        'Experiment', backref=db.backref('surveys'))

    def __unicode__(self):
        return self.question

    @property
    def admin_is_valid(self):
        '''
        Whether this survey is valid.
        '''
        return self.is_valid()

    @property
    def responders(self):
        '''
        Return users who responded to this survey.
        '''
        from clickadmin.users import User
        return (User.query
                .outerjoin(UserChoice, UserChoice.user_id==User.id)
                .filter(UserChoice.survey_id==self.id)
                .all())

    @property
    def responders_count(self):
        '''
        Return count of users who responded to this survey.
        '''
        return UserChoice.query.filter(UserChoice.survey_id==self.id).count()

    @property
    def nonresponders(self):
        '''
        Return users who did not respond to this survey.
        '''
        all_users = self.experiment.participating_users
        return list(set(all_users) - set(self.responders))

    @property
    def nonresponders_count(self):
        '''
        Return count of users who did not respond to this survey.
        '''
        total = self.experiment.participating_users_count
        return total - self.responders_count

    def is_valid(self):
        '''
        Surveys require at least two unique options to be valid.
        '''
        return len(self.options) > 1


class Option(Model, db.Model):

    __tablename__ = 'options'
    __table_args__ = (UniqueConstraint('survey_id', 'response'),)
    id = db.Column(db.Integer, primary_key=True)
    survey_id = db.Column(
        db.Integer, db.ForeignKey('surveys.id', ondelete='cascade'),
        nullable=False)
    response = db.Column(db.Unicode(140), nullable=False)

    survey = db.relationship(
        'Survey', backref=db.backref('options'))

    def __unicode__(self):
        return self.response

    @property
    def consenting_users(self):
        '''
        Return users who consented to this option.
        '''
        from clickadmin.users import User
        return (User.query
                .outerjoin(UserChoice, UserChoice.user_id==User.id)
                .filter(UserChoice.option_id==self.id)
                .all())

    @property
    def consenting_users_count(self):
        '''
        Return count of how many users consented to this option.
        '''
        return UserChoice.query.filter(UserChoice.option_id==self.id).count()


class Configuration(Model, db.Model):

    __tablename__ = 'configurations'
    id = db.Column(db.Integer, primary_key=True)
    previous_configuration_id = db.Column(
        db.Integer, db.ForeignKey('configurations.id'))
    experiment_id = db.Column(
        db.Integer, db.ForeignKey('experiments.id', ondelete='cascade'),
        nullable=False)
    text = db.Column(db.UnicodeText)
    duration = db.Column(db.Interval, nullable=False)

    previous_configuration = db.relationship(
        'Configuration', backref=db.backref('next_configuration', uselist=False),
        uselist=False, remote_side='Configuration.id')
    experiment = db.relationship(
        'Experiment', backref=db.backref('configurations'))

    def __unicode__(self):
        return unicode(self.id)

    @property
    def admin_duration(self):
        '''
        Natural representation of the duration.
        '''
        return naturaldelta(self.duration)

    @property
    def admin_is_valid(self):
        '''
        Whether this configuration is valid.
        '''
        return self.is_valid()

    @property
    def start_time(self):
        '''
        Return the computed start time of this configuration, based on start
        time of experiment and total duration of preceding configurations.
        '''
        start_time = self.experiment.start_time
        if not start_time:
            return
        td = timedelta(0)
        previous_configuration = self.previous_configuration
        while previous_configuration:
            td += previous_configuration.duration
            previous_configuration = previous_configuration.previous_configuration
        return start_time + td

    @property
    def end_time(self):
        '''
        Return the computed end time of this configuration, based on start
        time and duration of this configuration.
        '''
        start_time = self.start_time
        if start_time:
            return start_time + self.duration

    def render(self):
        '''
        Render the configuration template, passing in the necessary context.

        The context includes the experiment and several specially named variables
        representing each survey, and each survey option.

        Survey objects are specially named as survey_{sid}, e.g., survey_123.
        Option objects are specially named as option_{oid}, e.g., option_500.
        during
        @todo Find a safer way of passing objects directly to the template. For
              instance, rollback any and all changes to the session after
              rendering to void any possible changes to the db while rendering.
        '''
        if not self.text:
            return app.config['DEFAULT_CONFIGURATION']
        from jinja2 import StrictUndefined, Template, TemplateError
        context = dict(experiment=self.experiment)
        for survey in self.experiment.surveys:
            context['survey_%s' % survey.id] = survey
            for option in survey.options:
                context['option_%s' % option.id] = option
        try:
            return Template(self.text, undefined=StrictUndefined).render(**context)
        except TemplateError as e:
            app.logger.error('Configuration %s failed to render: %s' % (self.id, e))
            return None

    def is_valid(self):
        '''
        Configurations must render to be valid.
        '''
        if self.render() is None:
            return False
        return True


class UserChoice(Model, db.Model):

    __tablename__ = 'user_choices'
    __table_args__ = (UniqueConstraint('user_id', 'survey_id'),)
    user_id = db.Column(
        db.Integer, db.ForeignKey('users.id', ondelete='cascade'),
        primary_key=True, autoincrement=False)
    survey_id = db.Column(
        db.Integer, db.ForeignKey('surveys.id', ondelete='cascade'),
        primary_key=True, autoincrement=False)
    option_id = db.Column(
        db.Integer, db.ForeignKey('options.id', ondelete='cascade'),
        primary_key=True, autoincrement=False)

    user = db.relationship('User')
    survey = db.relationship('Survey')
    option = db.relationship('Option')


@event.listens_for(Session, 'before_flush')
def synchronize_schedule(session, flush_context, instances):
    '''
    Keep all scheduling information in sync, since the end time of an experiment
    is determined by the start time and the total duration of it's configurations.
    '''
    for action, objects in dict(append=session.new, remove=session.deleted, none=session.dirty).items():
        for obj in objects:
            if isinstance(obj, Experiment):
                experiment = obj
                configurations = set(experiment.configurations)
            elif isinstance(obj, Configuration):
                experiment = obj.experiment
                configurations = set(experiment.configurations)
                if action == 'append':
                    configurations.add(obj)
                elif action == 'remove':
                    configurations.remove(obj)
            else:
                continue
            experiment.duration = timedelta(0)
            for configuration in configurations:
                experiment.duration += configuration.duration
            if experiment.start_time:
                experiment.end_time = experiment.start_time + experiment.duration
            else:
                experiment.end_time = None


@event.listens_for(Session, 'before_flush')
def prevent_changes_to_past_experiments(session, flush_context, instances):
    '''
    For archiving purposes, prevent changes to past experiments.
    '''
    objects = list(session.new) + list(session.deleted) + list(session.dirty)
    for obj in objects:
        if isinstance(obj, Experiment):
            if obj.id:
                # Get existing copy
                experiment = Experiment.get(obj.id)
            else:
                # New object
                continue
        elif isinstance(obj, (Survey, Configuration)):
            experiment = obj.experiment
        elif isinstance(obj, (Option, UserChoice)):
            experiment = obj.survey.experiment
        else:
            continue
        if experiment.is_past():
            raise Exception('Cannot make changes to past experiment.')
        elif experiment.is_current():
            raise Exception('Cannot make changes to current experiment.')