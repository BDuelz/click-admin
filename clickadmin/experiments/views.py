from clickadmin.experiments.forms import AnswerSurveyForm
from clickadmin.experiments.models import Survey, Experiment
from flask import Blueprint, flash, redirect, render_template, url_for
from flask.ext.security import current_user, login_required


bp = Blueprint('experiments', __name__)


def index():
    experiments = Experiment.get_scheduled_experiments()
    return render_template('experiments/index.html',
                           experiments=experiments)


@bp.route('/experiment/<int:id>/')
def view_experiment(id):
    experiment = Experiment.get_or_404(id)
    return render_template('experiments/view_experiment.html',
                           experiment=experiment)


@bp.route('/experiment/<int:eid>/survey/<int:sid>/', methods=['GET', 'POST'])
@login_required
def answer_survey(eid, sid):
    experiment = Experiment.get_or_404(eid)
    return_to = url_for('.view_experiment', id=eid, _anchor='surveys')
    if not experiment.is_future():
        flash('Cannot answer experiment survey - too late.', 'warning')
        return redirect(return_to)
    survey = Survey.get_or_404(sid)
    form = AnswerSurveyForm()
    form.option_id.choices = [(o.id, o.response) for o in survey.options]
    if form.validate_on_submit():
        form.consent_to_option(current_user)
        flash('Survey answer saved successfully.', 'success')
        return redirect(return_to)
    return render_template('experiments/answer_survey.html',
                           survey=survey,
                           form=form)