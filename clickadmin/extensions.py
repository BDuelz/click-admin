from celery import Celery
from flask.ext.cache import Cache
from flask.ext.mail import Mail
from flask.ext.migrate import Migrate
from flask.ext.security import Security
from flask.ext.sqlalchemy import SQLAlchemy


# Flask-SQLAlchemy
db = SQLAlchemy()

# Flask-Cache
cache = Cache()

# Flask-Mail
mail = Mail()

# Flask-Migrate
migrate = Migrate()

# Flask-Security
security = Security()

# Celery
celery = Celery()