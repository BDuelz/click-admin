import logging
import os
import time
from clickadmin import experiments, frontend, users
from clickadmin.admin import configure_admin
from clickadmin.api import configure_api
from clickadmin.config import common
from clickadmin.extensions import cache, celery, db, mail, migrate, security
from datetime import datetime
from flask import Flask, flash, redirect, render_template, request, url_for
from flask.ext.security import SQLAlchemyUserDatastore, current_user
from flask_debugtoolbar import DebugToolbarExtension
from humanize import naturaltime
from humanize.time import naturaldelta
from logging import Formatter
from logging.handlers import RotatingFileHandler, SMTPHandler
from webhelpers.text import plural


DEFAULT_BLUEPRINTS = (
    experiments.bp,
    users.bp,
    frontend.bp,
)


def create_app(config=None, app_name=None, blueprints=None):
    if app_name is None:
        app_name = common.PROJECT_NAME
    if blueprints is None:
        blueprints = DEFAULT_BLUEPRINTS
    app = Flask(app_name,
                instance_path=common.INSTANCE_PATH,
                instance_relative_config=True)
    configure_app(app, config)
    configure_logging(app)
    configure_blueprints(app, blueprints)
    configure_routes(app)
    configure_extensions(app)
    configure_hooks(app)
    configure_context_processors(app)
    configure_template_filters(app)
    configure_error_handlers(app)
    return app


def configure_app(app, config):
    app.config.from_object(common)
    app.config.from_pyfile('config.py', silent=False)
    if config:
        app.config.from_object(config)
    os.environ['TZ'] = app.config['TIMEZONE']
    time.tzset()


def configure_logging(app):
    if app.debug or app.testing:
        return
    log_file = os.path.join(app.config['LOG_PATH'], '%s.log' % app.name)
    file_handler = RotatingFileHandler(log_file, maxBytes=100000, backupCount=10)
    file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(Formatter(
        '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'
    ))
    app.logger.addHandler(file_handler)
    mail_handler = SMTPHandler((app.config['MAIL_SERVER'], app.config['MAIL_PORT']),
                               app.config['MAIL_DEFAULT_SENDER'], app.config['ADMINS'],
                               'Click-Admin Error')
    mail_handler.setLevel(logging.ERROR)
    mail_handler.setFormatter(Formatter('''
        Message type:       %(levelname)s
        Location:           %(pathname)s:%(lineno)d
        Module:             %(module)s
        Function:           %(funcName)s
        Time:               %(asctime)s

        Message:

        %(message)s
    '''))
    app.logger.addHandler(mail_handler)


def configure_blueprints(app, blueprints):
    for blueprint in blueprints:
        app.register_blueprint(blueprint)


def configure_routes(app):
    @app.route('/')
    def index():
        if current_user.is_authenticated():
            return experiments.index()
        else:
            return frontend.index()


def configure_extensions(app):
    DebugToolbarExtension(app)
    cache.init_app(app)
    db.init_app(app)
    mail.init_app(app)
    migrate.init_app(app, db)
    celery.config_from_object(app.config)
    datastore = SQLAlchemyUserDatastore(db, users.User, users.Role)
    security.init_app(app, datastore)
    configure_admin(app)
    configure_api(app)


def configure_hooks(app):
    @app.before_request
    def before_request():
        if current_user.is_authenticated():
            message = None
            if not (current_user.ip_address and current_user.mac_address):
                message = 'Please enter your host information.'
            else:
                delta = datetime.now() - current_user.host_last_updated_at
                if delta > app.config['MAX_OUTDATED_HOST_TD']:
                    message = 'Please update your host information.'
            if message:
                endpoint = 'users.update_host'
                if request.endpoint not in (endpoint, 'security.logout', 'frontend.page'):
                    flash(message, 'warning')
                    return redirect(url_for(endpoint))


def configure_context_processors(app):
    @app.context_processor
    def data():
        now = datetime.now()
        frontend_pages = frontend.Page.all()
        current_experiment = experiments.Experiment.get_current_experiment()
        next_experiment = experiments.Experiment.get_next_experiment()
        return dict(now=now,
                    frontend_pages=frontend_pages,
                    current_experiment=current_experiment,
                    next_experiment=next_experiment,
                    current_user=current_user)


def configure_template_filters(app):
    app.add_template_filter(naturaltime, name='naturaltime')
    app.add_template_filter(naturaldelta, name='naturaldelta')
    app.add_template_filter(plural, name='pluralize')

    @app.template_filter()
    def datetimeformat(value, format='%Y-%m-%d %H:%M:%S'):
        return value.strftime(format)


def configure_error_handlers(app):
    if app.debug or app.testing:
        return

    def render_error(error):
        return render_template('errors/%s.html' % error.code), error.code

    for error in (403, 404, 500):
        app.register_error_handler(error, render_error)