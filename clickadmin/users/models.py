from clickadmin.extensions import db
from clickadmin.utils import Model
from flask.ext.security import RoleMixin, UserMixin
from sqlalchemy import event


class User(Model, UserMixin, db.Model):

    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.Unicode(255), nullable=False, unique=True)
    password = db.Column(db.CHAR(60), nullable=False)
    host_last_updated_at = db.Column(db.DateTime)
    active = db.Column(db.Boolean, nullable=False, default=True)
    last_login_at = db.Column(db.DateTime)
    current_login_at = db.Column(db.DateTime)
    last_login_ip = db.Column(db.Unicode(15))
    current_login_ip = db.Column(db.Unicode(15))
    login_count = db.Column(db.SmallInteger)

    def _get_ip_address(self):
        row = (self.ip_addresses
               .order_by(UserIpAddress.created_at.desc())
               .first())
        if row:
            return row.ip_address

    def _set_ip_address(self, ip_address):
        if ip_address != self.ip_address:
            UserIpAddress.create(user=self, ip_address=ip_address)

    ip_address = property(_get_ip_address, _set_ip_address)

    def _get_mac_address(self):
        row = (self.mac_addresses
               .order_by(UserMacAddress.created_at.desc())
               .first())
        if row:
            return row.mac_address

    def _set_mac_address(self, mac_address):
        if mac_address != self.mac_address:
            UserMacAddress.create(user=self, mac_address=mac_address)

    mac_address = property(_get_mac_address, _set_mac_address)

    def __unicode__(self):
        return self.email

    @classmethod
    def get_by_email(cls, email):
        return cls.query.filter(db.func.lower(cls.email)==db.func.lower(email)).first()

    def has_answered_all_surveys(self, experiment):
        '''
        Check whether this user has answered all surveys in a given experiment.
        '''
        for survey in experiment.surveys:
            if not self.has_answered_survey(survey):
                return False
        return True

    def _get_user_choice_model(self, survey):
        from clickadmin.experiments import UserChoice
        return (UserChoice.query
                .filter(UserChoice.user_id==self.id)
                .filter(UserChoice.survey_id==survey.id)
                .first())

    def has_answered_survey(self, survey):
        '''
        Check whether this user has answered the given survey.
        '''
        return bool(self._get_user_choice_model(survey))

    def get_consented_option(self, survey):
        '''
        Return the option this user consented to for the given survey.
        '''
        from clickadmin.experiments import Option, UserChoice
        return (Option.query
                .outerjoin(UserChoice, UserChoice.option_id==Option.id)
                .filter(UserChoice.user_id==self.id)
                .filter(UserChoice.survey_id==survey.id)
                .first())

    def has_consented_to_option(self, option):
        '''
        Check whether this user has consented to the given option.
        '''
        choice = self._get_user_choice_model(option.survey)
        return choice and choice.option is option

    def consent_to_option(self, option):
        '''
        Set the given option as the users response to it's survey.
        '''
        choice = self._get_user_choice_model(option.survey)
        if choice:
            choice.option = option
        else:
            from clickadmin.experiments import UserChoice
            UserChoice.create(user=self, survey=option.survey, option=option)


class UserIpAddress(Model, db.Model):

    __tablename__ = 'user_ip_addresses'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
        db.Integer, db.ForeignKey('users.id', ondelete='cascade'), nullable=False)
    ip_address = db.Column(db.Unicode(39), nullable=False)

    user = db.relationship(
        'User', backref=db.backref('ip_addresses', lazy='dynamic'), lazy='joined')

    def __unicode__(self):
        return self.ip_address


class UserMacAddress(Model, db.Model):

    __tablename__ = 'user_mac_addresses'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(
        db.Integer, db.ForeignKey('users.id', ondelete='cascade'), nullable=False)
    mac_address = db.Column(db.Unicode(17), nullable=False)

    user = db.relationship(
        'User', backref=db.backref('mac_addresses', lazy='dynamic'), lazy='joined')

    def __unicode__(self):
        return self.mac_address


class Role(Model, RoleMixin, db.Model):

    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(80), nullable=False, unique=True)
    description = db.Column(db.Unicode(255))
    permanent = db.Column(db.Boolean, nullable=False, default=False)

    users = db.relationship(
        'User', backref=db.backref('roles', lazy='joined'),
        secondary='users_roles')

    def __unicode__(self):
        return self.name

    @classmethod
    def get_by_name(cls, name):
        return cls.query.filter(cls.name==name).one()


class UserRole(Model, db.Model):

    __tablename__ = 'users_roles'
    user_id = db.Column(
        db.Integer, db.ForeignKey('users.id', ondelete='cascade'),
        primary_key=True, autoincrement=False)
    role_id = db.Column(
        db.Integer, db.ForeignKey('roles.id', ondelete='cascade'),
        primary_key=True, autoincrement=False)


@event.listens_for(Role, 'before_delete')
def prevent_delete(mapper, connection, target):
    if target.permanent:
        raise Exception('%s role is permanent.' % target.name.capitalize())