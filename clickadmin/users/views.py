from clickadmin.users.forms import UpdateEmailForm, UpdateHostForm
from flask import Blueprint, flash, redirect, render_template, url_for
from flask.ext.security import current_user, login_required


bp = Blueprint('users', __name__)


@bp.route('/email/', methods=['GET', 'POST'])
@login_required
def update_email():
    form = UpdateEmailForm(current_user.email, obj=current_user)
    if form.validate_on_submit():
        form.update_email(current_user)
        flash('Email updated successfully.', 'success')
        return redirect(url_for('index'))
    return render_template('users/email.html', form=form)


@bp.route('/host/', methods=['GET', 'POST'])
@login_required
def update_host():
    form = UpdateHostForm(obj=current_user)
    if form.validate_on_submit():
        form.update_host(current_user)
        flash('Host updated successfully.', 'success')
        return redirect(url_for('index'))
    return render_template('users/host.html', form=form)