from datetime import datetime
from flask.ext.security.forms import unique_user_email
from flask.ext.wtf import Form
from wtforms import SubmitField, TextField
from wtforms.validators import Email, IPAddress, MacAddress, Required


class UpdateEmailForm(Form):
    
    email = TextField('Email', validators=[Required(), Email()])
    submit = SubmitField('Update Email')
    
    def __init__(self, current_email, *args, **kwargs):
        super(UpdateEmailForm, self).__init__(*args, **kwargs)
        self.current_email = current_email
    
    def validate_email(self, field):
        if field.data != self.current_email:
            return unique_user_email(self, field)
    
    def update_email(self, user):
        user.email = self.email.data


class UpdateHostForm(Form):
    
    ip_address = TextField('Host IP address', validators=[Required(), IPAddress(ipv4=True, ipv6=True)],
        description='IP address of machine you plan to use.')
    mac_address = TextField('Host MAC address', validators=[Required(), MacAddress()],
        description='MAC address of machine you plan to use.')
    submit = SubmitField('Update Host')
    
    def update_host(self, user):
        user.ip_address = self.ip_address.data
        user.mac_address = self.mac_address.data
        user.host_last_updated_at = datetime.now()