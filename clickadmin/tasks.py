import os
import subprocess
from celery.signals import task_postrun
from clickadmin.experiments import Experiment
from clickadmin.extensions import celery, db, mail
from datetime import datetime, timedelta
from flask import render_template, current_app as app
from flask.ext.mail import Message


@task_postrun.connect
def close_session(*args, **kwargs):
    db.session.remove()


@celery.task(name='check-next-experiment-for-errors')
def check_next_experiment_for_errors():
    '''
    Send out an email if there is an error with the next experiment.
    '''
    experiment = Experiment.get_next_experiment()
    if not experiment:
        return
    delta = experiment.start_time - datetime.now()
    if delta > app.config['FIRST_NOTIFICATION_TD']:
        return
    errors = []
    if not experiment.has_valid_surveys():
        errors.append('invalid-surveys')
    if not experiment.has_valid_configurations():
        errors.append('invalid-configurations')
    if not experiment.has_valid_configuration_order():
        errors.append('invalid-configuration-order')
    if errors:
        html = render_template('emails/experiment_error.html',
                               experiment=experiment,
                               errors=errors)
        message = Message('There are errors with an upcoming experiment',
                          recipients=app.config['ADMINS'], html=html)
        mail.send(message)


@celery.task(name='send-survey-reminders')
def send_survey_reminders():
    '''
    Send reminders to each user who has not yet responded to all surveys
    in the next experiment.
    '''
    experiment = Experiment.get_next_experiment()
    if not experiment:
        return
    delta = experiment.start_time - datetime.now()
    td = timedelta(minutes=15)
    if delta > app.config['FIRST_NOTIFICATION_TD'] + td:
        return
    if (app.config['FIRST_NOTIFICATION_TD'] - td < delta < app.config['FIRST_NOTIFICATION_TD'] + td or
        app.config['SECOND_NOTIFICATION_TD'] - td < delta < app.config['SECOND_NOTIFICATION_TD'] + td):
        recipients = []
        for user in experiment.participating_users:
            if not user.has_answered_all_surveys(experiment):
                recipients.append(user)
        if recipients:
            html = render_template('emails/survey_reminder.html', experiment=experiment)
            message = Message('Please answer all surveys for next trial',
                              recipients=recipients, html=html)
            mail.send(message)


@celery.task(name='update-router-configuration')
def update_router_configuration():
    '''
    Starts the router with the next configuration.
    '''
    experiment = Experiment.get_current_experiment()
    if not experiment:
        return
    now = datetime.now()
    td = timedelta(minutes=15)
    filename = app.config['ROUTER_CONFIG_FILE']
    for configuration in experiment.ordered_configurations:
        if now - td < configuration.end_time < now + td:
            subprocess.Popen(['sudo', 'supervisorctl', 'stop', 'click'])
            if os.path.exists(filename):
                os.remove(filename)
        elif now - td < configuration.start_time < now + td:
            with open(filename, 'w') as f:
                f.write(configuration.render())
            subprocess.Popen(['sudo', 'supervisorctl', 'start', 'click'])