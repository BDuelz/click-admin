from flask.ext.restful import Api


def configure_api(app):
    v1 = Api(app, prefix='/api')