from clickadmin.experiments import Configuration, Experiment, Option, Survey
from clickadmin.extensions import db
from clickadmin.users import User
from factory.alchemy import SQLAlchemyModelFactory


class BaseFactory(SQLAlchemyModelFactory):

    FACTORY_SESSION = db.session


class UserFactory(BaseFactory):

    FACTORY_FOR = User


class ExperimentFactory(BaseFactory):

    FACTORY_FOR = Experiment


class SurveyFactory(BaseFactory):

    FACTORY_FOR = Survey


class OptionFactory(BaseFactory):

    FACTORY_FOR = Option


class ConfigurationFactory(BaseFactory):

    FACTORY_FOR = Configuration