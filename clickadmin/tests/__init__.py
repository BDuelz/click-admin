from clickadmin import create_app
from clickadmin.config import testing
from clickadmin.extensions import db
from flask.ext.testing import TestCase


class BaseTestCase(TestCase):

    def create_app(self):
        app = create_app(testing)
        return app

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def login(self, **kwargs):
        return self.client.post('/login', data=kwargs, follow_redirects=True)

    def logout(self):
        return self.client.get('/logout', follow_redirects=True)