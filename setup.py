import multiprocessing
import os
from setuptools import setup, find_packages


def read(*path):
    with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), *path)) as f:
        return f.read()


setup(
    name='click-admin',
    version='0.1.0',
    url='https://bitbucket.org/BDuelz/click-admin',
    author='Abdul Hassan',
    author_email='abdulrhass@gmail.com',
    description='An easy to use interface to Click Modular Router.',
    long_description=read('README.txt'),
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    test_suite='nose.collector',
    tests_require=['nose'],
)