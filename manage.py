from clickadmin import create_app, frontend, users
from clickadmin.extensions import db
from flask import current_app as app, request
from flask.ext.migrate import MigrateCommand
from flask.ext.script import Manager
from flask.ext.script.commands import Clean, ShowUrls
from flask.ext.security.registerable import register_user
from sqlalchemy.exc import IntegrityError


defaults = {
    frontend.Page: [
        dict(name='home', title='Home', permanent=True),
        dict(name='terms', title='Terms of Service', permanent=True),
        dict(name='privacy', title='Privacy Policy', permanent=True),
    ],
    users.Role: [
        dict(name='admin', description='Administrator', permanent=True),
    ],
}

manager = Manager(create_app)
manager.add_option('-c', '--config', required=False)
manager.add_command('clean', Clean())
manager.add_command('urls', ShowUrls())
manager.add_command('db', MigrateCommand)


@manager.shell
def make_context():
    return dict(app=app, request=request, db=db)


@MigrateCommand.command
def seed():
    '''
    Seed the database with necessary data.
    '''
    for model, rows in defaults.iteritems():
        for row in rows:
            try:
                m = model(**row)
                db.session.add(m)
                db.session.commit()
            except IntegrityError:
                db.session.rollback()
    for email in app.config['ADMINS']:
        try:
            register_user(email=email, password='password',
                          roles=[users.Role.get_by_name('admin')])
        except IntegrityError:
            db.session.rollback()


if __name__ == '__main__':
    manager.run()
